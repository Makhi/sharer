#Mastodon Share

##1.2.3

* Refactorización del Javascript
* Mejorada la estructura con directorios
* Añadido Manifest.webmanifest
* Añadida versión del software
* Corregido error con ventana de Hubzilla

##1.2.4
* Añadido parametro url del webservice para actualizar clicks en la base de datos

##1.2.5
* Añadidos Friendica y Movim

##1.2.5.M1
* Reorganizadas las cajas
* Cambiado logo de Diaspora
* Añadidas traducciónes en Castellano y Catalán
* La traducción al Castellano de index.html reemplaza a la de Inglés en el directorio principal, siendo movida esta última a un subdirectorio
* index.html en Catalán e Inglés funcionan perfectamente desde sus respectivos directorios sin necesidad de ser movidos al directorio principal
* Renombrado sharer.css a light_sharer.css y añadido dark_sharer.css para hoja de estilo oscura

##1.2.5.M2
* Añadido Pleroma
* Añadidas hojas de estilo, sepia (sepia_sharer.css) y violeta (violet_sharer.css)